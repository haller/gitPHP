<?php
namespace Controller;

require 'config.php';

use Classes\db\MyDatabase;
use Classes\MyError\MyCustomError;
use Classes\MyError\MyError;

class ManipulationDatabase
{

  private $mydb;
  private $tableName;

  private function instanceTradeDatabase (  )
  {

    $this->mydb = new MyDatabase (  );
    $this->tableName = 'test';
  }

  public function deleteIntoDatabase ( $arguments )
  {

    $this->instanceTradeDatabase (  );

    $whereAgument = array (

      $arguments [ 'camp' ] => $arguments [ 'value' ]
    );

    $this->mydb->deleteIntoTable ( $this->tableName, $whereAgument );

  }

  public function insertIntoDatabase ( $arguments )
  {

    $this->instanceTradeDatabase (  );

    $arrayValuesFields = array (

      'name' => $arguments [ 'argv' ][ 'name' ],
      'password' => $arguments [ 'argv' ][ 'password' ]
    );

    $this->mydb->insertIntoTable ( $this->tableName, $arrayValuesFields );

  }

  public function showIntoDatabase ( $arguments )
  {

    $this->instanceTradeDatabase (  );

    $whereAgument = array (  );

    if ( !( is_null ( $arguments ) ) )
      $whereAgument = array (

        'argv' => array (

          $arguments [ 'argv' ]['camp'] => $arguments [ 'argv' ]['value']
        )
      );

    return json_encode ( $this->mydb->selectTable ( $this->tableName, $whereAgument['argv'] ) );
  }

  public function updateIntoDatabase ( $arguments )
  {

    $this->instanceTradeDatabase (  );

    $arrayValuesFields = array (

      'name' => $arguments [ 'argv' ][ 'name' ],
      'password' => $arguments [ 'argv' ][ 'password' ]
    );

    $whereAgument = array (  );

    if ( !( is_null ( $arguments [ 'where' ] ) ) )
      $whereAgument = array (

        'argv' => array (

          $arguments [ 'argv' ]['camp'] => $arguments [ 'argv' ]['value']
        )
      );

    $this->mydb->updateTable ( $this->tableName, $arrayValuesFields, $whereAgument );

  }

  public function __construct (  )
  {

    $this->instanceTradeDatabase (  );
  }

  public function __destructIntoDatabase (  )
  {

    $this->mydb = null;
    $this->tableName = null;
  }
}
