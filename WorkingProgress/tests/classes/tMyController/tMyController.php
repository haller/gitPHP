<?php
namespace Testes\tMyController;

use Classes\MyError\MyCustomError;
use Classes\MyError\MyError;

use Classes\db\MyDatabase;

use Controller\Controller;

class tMyController
{



  public function internalTest (  )
  {

    try {

      ( new Controller (  ) )->authUserVerify (  );

    } catch (\Exception $error) {

      ( new MyError (  ) )->checkTypeError ( $error );
    }


  }

  public function seguenceStarter (  )
  {

    $this->internalTest (  );
  }
}
