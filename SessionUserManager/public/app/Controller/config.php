<?php

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'bootstrap.php';

if ( ERROR_REPORTING ) {

  ini_set ( 'display_errors', 1 );
  ini_set ( 'display_startup_erros', 1 );
  error_reporting ( E_ALL );
}
