<?php
namespace Classes\MyError;

use Classes\MyError\MyError;

class MyCustomError extends MyError
{

  public function __construct ( $flag = MyError::E_UNDEFINED_ERROR, $message='' )
  {

    $this->message = $message . '.FLAG: ' . array_keys ( $this->switchError ( $flag ) )[0];
    $this->code = $flag;
  }

}
