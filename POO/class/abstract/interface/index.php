<?php

interface Veiculo {

  public function acelerar ( $velocidade );
  public function frenar ( $velocidade );
  public function trocarMarcha ( $marcha );

}

interface Acao {

  public function falar (  );
  public function mover (  );
}
