<?php

require_once ( "abstract/index.php" );

class Cachoro extends Animal {

  public function falar (  ) {

    return "Late";
  }
}

class Gato extends Animal {

  public function falar (  ) {

    return "Mia";
  }
}

class Ave extends Animal {

  public function falar (  ) {

    return "Canta";
  }
  
  public function mover (  ) {

    return "Voa e " . parent::mover (  );
  }
}

class Carro extends Automovel {

  private $farou;

  public function getFarou (  ) {

    return $this->farou;
  }

  public function setFarou ( $farou ) {

    $this->farou = $farou;
  }
}
class Documento {

  protected $titulo;
  protected $data;

  public function __construct ( $titulo = "", $data = "" ) {

    $this->titulo = $titulo;
    $this->data = $data;
  }

  public function setTitulo ( $titulo ) {

    $this->titulo = $titulo;
  }

  public function getTitulo (  ): string {

    return $this->titulo;
  }

  public function setData ( $data ) {

    $this->data = $data;
  }

  public function getData (  ): string {

    return $this->data;
  }
};

class Numero extends Documento {

  protected $numero;

  public function getNumero (  ) {

    return $this->numero;
  }

  public function setNumero ( $Numero ) {

    $this->numero = $Numero;
  }

  private static function calculo ( $auxiliar = 1) {

    return json_encode ( array (
      'x' => $auxiliar,
      'x2' => ( $auxiliar * 2 ),
      'x+' => ( $auxiliar + 1 )
    ) );
  }
}

class Endereco extends Numero {

  public $rua;
  public $cidade;
  public $numeroCasa;

  public function __construct ( $rua, $cidade, $numeroCasa ) {

    $this->rua = $rua;
    $this->cidade = $cidade;
    $this->numeroCasa = $numeroCasa;
  }

  public function __destruct (  ) {

    // echo "destroy object";
  }

  public function getAll (  ) {

    return array (
      'rua' => $this->rua,
      'cidade' => $this->cidade,
      'numero' => $this->numeroCasa
    );
  }
};

class Pessoa extends Endereco {

  public $nomePessoal = "Rasmus Lerdof";
  protected $idade = 42;
  private $senha = "23";

  public function verDados ( $separador = " - " ) {

    return $this->nome . $separador . $this->idade . $separador . $this->senha;
  }
};

class Cpf extends Pessoa {

  public function __construct (  $titulo="CPF", $numero = 0, $nomePessoal = "", $senha = "",
                                   $data = "", $rua = "", $cidade = "Fortaleza",
                                            $numeroCasa = 0, $idade = 0) {

    $this->titulo = $titulo;
    $this->numero = $numero;
    $this->data = $data;
    $this->rua = $rua;
    $this->cidade = $cidade;
    $this->numeroCasa = $numeroCasa;
    $this->nomePessoal = $nomePessoal;
    $this->idade = $idade;
    $this->senha = $senha;
  }
}

class Programador extends Cpf {

  public function verInformacoesGerais ( ) {

    return array (

      "titulo" => $this->titulo,
      "numero" => $this->numero,
      "data" => $this->data,
      "rua" => $this->rua,
      "cidade" => $this->cidade,
      "numeroCasa" => $this->numeroCasa,
      "nomePessoal" => $this->nomePessoal,
      "idade" => $this->idade,
      "senha" => $this->senha
    );
  }

  public function classeObjeto (  ) {
    return get_class ( $this ) . "<br/>";
  }
};
