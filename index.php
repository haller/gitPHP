<?php

//  phpinfo (    );
/*
$nome = "NOME";

echo $nome;

echo "<br/>";

var_dump ( $nome );
////////////////////////////////////////////////////////////////////////
// ##### CamelCase


$anoNascimento = 1999;

// ERROR
// $1nome

$nome1 = "Eliseu <br/>";

echo "Ano Nascimento : ", $anoNascimento, "<br/>";

echo "Nome : ", $nome1, "<br/>";
////////////////////////////////////////////////////////////////////////
$informacoesPessoais = $nome1 . " " . $anoNascimento;

echo "Informacoes Pessoais: ", $informacoesPessoais, "<br/>";

exit;
////////////////////////////////////////////////////////////////////////
// unset (  $nome, $anoNascimento  );
unset (  $nome, $anoNascimento  );

echo "<br/>";

if  (  isset  (  $nome  ) && isset  (  $anoNascimento  )  ) {

  echo "Ano Nascimento : ", $anoNascimento, "<br/>";

  echo "Nome : ", $nome1, "<br/>";
}

////////////////////////////////////////////////////////////////////////

$nome = "Hcode";
$site = 'www.site.com.br';

$ano = 1990;
$salario = 5500.99;
$bloqueio = true;
/////////////////////// :# Array
$frutas = array ( "Abacaxi" ,"Manga", "Cereja"  );

echo $frutas, "<br/>";

var_dump ( $frutas );

$nascimento = new DateTime (  );

var_dump ( $nascimento );

echo "<br/>";
////////////////////////////////////////////////////////////////////////


echo "<H1> SERVER </H1>";
$server = $_SERVER["REMOTE_ADDR"];
echo $server, "<BR/>";
var_dump ( $server );

echo "<br/> <br/> <br/>";

echo "<H1> GET </H1>";
$get = $_GET["a"];
echo $get, "<BR/>";
var_dump ( $get );

echo "<br/> <br/> <br/>";
echo "<br/>";
////////////////////////////////////////////////////////////////////////



$ola = "ola";

$ola .= " " . "Mundo";

echo $ola;

echo "<br>";

$total = 0;

$total += 60;

$total += 50;

$total *= .5;

echo $total;

$a = 5;

$b = 6;

echo "<br/>";
echo $a * $b;
echo "<br/>";
echo $a - $b;
echo "<br/>";
echo $a + $b;
echo "<br/>";
echo $a % $b;
echo "<br/>";
echo $a / $b;
echo "<br/>";
echo $a ** $b;

echo "<br/>";
var_dump ( $a < $b );
echo "<br/>";
var_dump ( $a > $b );
echo "<br/>";
var_dump ( $a >= $b );
echo "<br/>";
var_dump ( $a <= $b );
echo "<br/>";
var_dump ( $a != $b );
echo "<br/>";
var_dump ( $a !== $b );
echo "<br/>";
var_dump ( $a == $b );
echo "<br/>";
var_dump (  (  ( int ) ( '5' ) ) === 5 );

echo "<br/>";
var_dump ( 1 <=> 0 );
echo "<br/>";
var_dump ( 1 <=> 1 );
echo "<br/>";
var_dump ( 0 <=> 1 );

$a = NULL;
$b = NULL;
$c = 11;

echo "<br/>";
echo $a ?? $b ?? $c;
echo "<br/>";
echo $a ?? 5 ?? $c;


$f = 11;

echo "<br/>";
echo $f . "<br/>";
echo $f++ . "<br/>";
echo $f . "<br/>";

echo "<br/>";
echo $f . "<br/>";
echo $f-- . "<br/>";
echo $f . "<br/>";

echo "<br/>";
echo $f . "<br/>";
echo ++$f . "<br/>";
echo $f . "<br/>";

echo "<br/>";
echo $f . "<br/>";
echo --$f . "<br/>";
echo $f . "<br/>";

echo "<br/>";
var_dump ( true && true );
echo "<br/>";
var_dump ( true && false );
echo "<br/>";
var_dump ( false && true );
echo "<br/>";
var_dump ( false && true );

echo "<br/>";
var_dump ( true || true );
echo "<br/>";
var_dump ( true || false );
echo "<br/>";
var_dump ( false || true );
echo "<br/>";
var_dump ( false || false );

////////////////////////////////////////////////////////////////////////


$nome = "nome";
$casa = 'home';

echo "<br/>";
var_dump ( $nome );
echo "<br/>";
var_dump ( $casa );

echo "<br/>";
echo "meu nome é $nome";

echo "<br/>";
echo strtoupper($nome);

echo "<br/>";
echo strtolower($nome);

echo "<br/>";
echo ucfirst($nome);
echo "<br/>";
echo ucwords($nome . " casa");

$empresa = "opovo";
$empresa = str_replace("o", "0", $empresa);
$empresa = str_replace("v", "w", $empresa);
$empresa = str_replace("p", "b", $empresa);

echo "<br/>";
echo ucwords($empresa);

$frase = "A necessidade é a mãe da invenção.";
$buscaFrase = "mãe";

$possicaoBuscaMae = strpos($frase, $buscaFrase);

echo "<br/>";
echo $possicaoBuscaMae;

$textoCortadoInicio = substr($frase, 0, $possicaoBuscaMae);

echo "<br/>";
echo $textoCortadoInicio;

$textoCortadoFim = substr($frase, $possicaoBuscaMae + strlen($buscaFrase), strlen($frase));

echo "<br/>";
echo $buscaFrase;

echo "<br/>";
echo $textoCortadoFim;

////////////////////////////////////////////////////////////////////////


$idadePessoa = 42;

if ( $idadePessoa < 18 ) {

  echo "Crianca";
} else if ( $idadePessoa < 65 ) {

  echo "Adulto";
} else {

  echo "Melhor Idade";
};

////////////////////////////////////////////////////////////////////////


$diaCorespondenteSemana = date("w");

switch ($diaCorespondenteSemana) {

  case 0:
    echo "Domingo";
    break;

  case 1:
    echo "Segunda-Feira";
    break;

  case 2:
    echo "Terca-Feira";
    break;

  case 3:
    echo "Quarta-Feira";
    break;

  case 4:
    echo "Quinta-Feira";
    break;

  case 5:
    echo "Sexta-Feira";
    break;

  case 6:
    echo "Sabado";
    break;

  default:
    echo "Nenhuma opção encontrada.";
    break;
}

////////////////////////////////////////////////////////////////////////


for ( $i=0; $i < 10; $i++ ) {

  echo $i . "<br/>";
}

echo "date: " . date ( "Y" ) . "<br/>";

echo "<select>";

for ( $i = date ( "Y" ); $i > ( date ( "Y" ) - 100 ); $i-- ) {

  echo '<option value="'. $i .'">'. $i .'</option>';
}

echo "</select>";

////////////////////////////////////////////////////////////////////////


$mesesDoAno = array (
  "Janeiro", "Fevereiro", "Março",
  "Abril", "Maio", "Junho",
  "Julho", "Agosto", "Setembro",
  "Outubro", "Novembro", "Dezembro"
);

foreach ( $mesesDoAno as $possicaoMes => $mesAtual ) {

  echo "O Mês ". ++$possicaoMes ." é: ". $mesAtual ."<br/>";
}

echo "OLA!";

echo "<form>";

  echo '<imput type="text" name="nome"></imput>';
  echo '<imput type="date" name="nascimento"></imput>';
  echo '<imput type="submit" value="ENVIAR"></imput>';

echo "</form>";


if ( isset ( $_GET ) ) {

  foreach ( $_GET as $key => $value ) {

    echo "Valor do campo ". $key .": ". $value ."<br/><hr/>";
  }
}

////////////////////////////////////////////////////////////////////////


$coisas[0][0] = "veicolos";
$coisas[0][1] = "moto";
$coisas[0][2] = "caminhao";
$coisas[0][3] = "carro";

$coisas[1][0] = "computador";
$coisas[1][1] = "processador";
$coisas[1][2] = "hd";
$coisas[1][3] = "placa de video";

print_r ( $coisas[0][3] );
echo "<br/>";
echo end ( $coisas[1] );

$pessoas = array (  );

array_push ( $pessoas, array (
  'nome' => "Eliseu",
  'cidade' => "fortaleza",
  'CEP' => 60356265,
  'idade' => 19
) );

array_push ( $pessoas, array (
  'nome' => "Carou",
  'cidade' => "fortaleza",
  'CEP' => 60356268,
  'idade' => 19
) );
// echo "<br/>";
// print_r($pessoas[0]['nome']);
echo json_encode ( $pessoas );

$json = '[{"nome":"Eliseu","cidade":"fortaleza","CEP":60356265,"idade":19},{"nome":"Carou","cidade":"fortaleza","CEP":60356268,"idade":19}]';

$data = json_decode ( $json, true );

var_dump ( $data );

////////////////////////////////////////////////////////////////////////



define ( "CASA", 23 );
define ( "SERVER_CONECT", [
    "ip" => "127.0.0.1",
    "user" => "root",
    "port" => "5747",
    "database" => "DATABASE"
] );

echo CASA . "<br/>";

echo SERVER_CONECT['ip'];

////////////////////////////////////////////////////////////////////////


function ola ( $texto, $periodo = "Bom Dia!" ) {

  return "ola $texto! $periodo!<br/>";
}

function args (  ) {
  return func_get_args (  );
}

echo ola ( "eliseu" );
echo ola ( "" );
echo ola ( "","Boa Tarde" );
echo ola ( "Carla", "Boa Noite" );

echo json_encode ( args ( "", 123, "asdf", args ) );

echo "<br/><br/><br/>";

$valorEscorpoGlobal = 10;

function variable ( &$VariavelDeParametro ) {
  return ( $VariavelDeParametro += 50 );
}

echo variable ( $valorEscorpoGlobal );
echo "<br/>";
echo variable ( $valorEscorpoGlobal );
echo "<br/>";
echo variable ( ( $valorEscorpoGlobal += 6 ) );
echo "<br/>";
echo variable ( ( $valorEscorpoGlobal += 6 ) );

echo "<br/><br/>";

$pessoa = array (
  'nome' => "Eliseu",
  'idade' => 19
);
echo "<br/>";

echo json_encode ( $pessoa );

foreach ( $pessoa as &$value ) {
  if ( gettype ( $value ) === "integer" ) $value += 5;
  echo "<br/>" . $value . "<br/>";
}

echo json_encode ( $pessoa );

echo "<br/><br/>";

function soma ( int ...$numeros ): String {
  return array_sum ( $numeros );
}

echo var_dump ( soma ( 3, 4, 5, 6, 7, 7 ) );
echo "<br/>";
echo soma ( 3, 4, 5.4 );
echo "<br/>";
echo soma ( 3, 2, 7 );
echo "<br/>";
echo soma ( 1.7, 2.4 );
echo "<br/>";

echo "<br/><br/><br/>";

$cargos = array (
  'nome_cargo' => 'CEO',
  'subordinado' => array (
    'nome_cargo' => 'Gerente',
    'subordinado' => array (
      'nome_cargo' => 'Gerente de Vendas',
      'subordinado' => array (
        'nome_cargo' => 'gerente de Financeiro'
      )
    ),
    'subordinado' => array (
      'nome_cargo' => 'Analista',
      'subordinado' => array (
        'nome_cargo' => 'TI'
      )
    )
  )
);

function htmlViewArray ( $arrayVector, $defaultValue, $subLevelDefault ) {

  $html .= '<ul>';

  foreach ( $arrayVector as $value ) {

    $html .= '<li>';

    $html .= $value [ $defaultValue ];

    if ( isset ( $value [ $subLevelDefault ] ) && count ( $value [ $subLevelDefault ] ) > 0 )
      $html .= htmlViewArray ( $value [ $subLevelDefault ], $defaultValue, $subLevelDefault );

    $html .= '</li>';

  }

  $html .= '<ul>';

  return $html;

}

echo htmlViewArray ( $cargos, 'nome_cargo', 'subordinado' );

echo "<br/><br/><br/>";

function testCallbackTalk ( $processRetornCallback ) {
  $processRetornCallback (  );
}

testCallbackTalk ( function (  ) { echo "OLA"; } );


////////////////////////////////////////////////////////////////////////
*/
