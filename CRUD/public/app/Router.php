<?php
namespace App;

use Classes\db\MyDatabase;

use Classes\MyError\MyCustomError;
use Classes\MyError\MyError;

class Router
{

  private function serverRequisition (  )
  {

    if ( $_SERVER [ 'REQUEST_METHOD' ] == 'GET' ) {

      // echo "get method";
      // echo json_encode ($_GET);
      header( 'location: ' . '..'. DS . 'views' . DS . 'index.php' );
      die (  );
    }

    header( 'location: /' );

  }

  private function internalTests (  )
  {

    $this->serverRequisition (  );
    // echo '<pre>';
    // echo json_encode ($_SERVER);

  }

  public function checkRequisition (  )
  {

    $this->internalTests (  );
  }

}
