<?php
namespace Testes\tMyError;

use Classes\MyError\MyCustomError;
use Classes\MyError\MyError;

class tMyError
{

  public function createError ( $flag = ( MyError::E_INVALID_USER ) )
  {

    throw new MyCustomError ( $flag );
  }

  public function createErrorMessage ( $flag = ( MyError::E_INVALID_USER ), $message )
  {

    throw new MyCustomError ( $flag, $message );
  }

  public function testBasic (  )
  {

    try {

      $this->createError ( MyError::E_INVALID_USER );
    } catch ( \Exception $error ) {

      $error->checkTypeError ( $error );
    }
  }

  public function myCustomErrorMessageTest (  )
  {

    try {

      $this->createErrorMessage ( MyError::E_INVALID_USER, "MESSEGE BLABLA" );
    } catch ( \Exception $error ) {

      $error->checkTypeError ( $error );
    }
  }

  public function testSequece (  )
  {

    try {

      $this->createError ( MyError::E_INVALID_USER );
    } catch ( \Exception $error ) {

      $error->checkTypeError ( $error );
    }
  }
}
