<?php
namespace Classes\db;

use Classes\db\DatabaseInterface;

use Classes\MyError\MyCustomError;
use Classes\MyError\MyError;

use PDO;

class MyDatabase implements DatabaseInterface
{

  private $conection;
  private $hostHospededBase;
  private $userName;
  private $passwdLoginUser;
  private $databaseType;
  private $stdDatabase;

  protected function createObjectConection (  )
  {

    return new \PDO ( "$this->databaseType:host=$this->hostHospededBase", "$this->userName", "$this->passwdLoginUser" );
  }

  protected function createDatabaseObjectConection (  )
  {

    return new \PDO ( "$this->databaseType:dbname=$this->stdDatabase; host=$this->hostHospededBase", "$this->userName", "$this->passwdLoginUser" );
  }

  public function desconectDatabase (  )
  {

    $this->conection = null;
  }

  public function conectDatabase ( $conectionDatabase = true )
  {

    try {

      if ( $conectionDatabase )
        $this->conection = $this->createDatabaseObjectConection (  );
      else
        $this->conection = $this->createObjectConection (  );

      $this->conection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      if ( !( $this->conection->errorCode (  ) ) )
         throw new MyCustomError ( MyError::E_BAD_CONNECTION_ERROR, $this->conection->errorCode (  ) );

    } catch ( \Exception $error ) {

      ( new MyError )->checkTypeError ( $error );
      die (  );
    }
  }

  public function resetConection ( $databaseConect = true, $testDatabase = false )
  {
    $this->desconectDatabase (  );

    if ( $testDatabase )
      $this->checkDatabase (  );

    $this->conectDatabase ( $databaseConect );
  }

  public function queryTransaction ( $query, $elements = array (  ), $desconectDatabase = true, $beginDatabase = true )
  {

    if ( is_null ( $this->conection ) ) {

      $this->conectDatabase ( $beginDatabase );
    }

    $this->conection->beginTransaction (  );

    try {

      $statements = ( $this->conection )->prepare( "$query" );

      $statements->execute( $elements );

      $this->conection->commit (  );

      if ( $desconectDatabase )
        $this->desconectDatabase (  );

      if ( $statements->columnCount (  ) )
        return $statements->fetchAll (  );

    } catch ( \Exception $error ) {

      ( new MyError )->checkTypeError ( $error );

      $this->conection->rollback (  );

    }

    return NULL;
  }

  private function separeteFieldsAndValues ( $fieldsValued )
  {

    $fields = ' ( ';
    $values = ' ( ';

    foreach ( $fieldsValued as $field => $value ) {

      $fields .= $field . ' , ';
      $values .= '\'' . $value . '\' , ';
    }

    $fields = substr ( $fields , 0 , -3 );
    $values = substr ( $values , 0 , -3 );

    $fields .= ' ) ';
    $values .= ' ) ';

    return array (

      $fields,
      $values
    );
  }

  private function clusterFieldsAndValues ( $fieldsValued )
  {

    $fields = '';

    foreach ( $fieldsValued as $field => $value ) {

      $fields .= $field . ' = ' . '\'' . $value . '\', ';
    }

    $fields = substr ( $fields , 0 , -2 );

    return $fields;
  }

  private function whereCondition ( $whereCondition )
  {

    $queryWhereArguments = " WHERE ";

    if ( count ( $whereCondition ) - 1 ) {

      $args = $this->separeteFieldsAndValues ( $whereCondition );
      $queryWhereArguments .= $args[0] . ' = ' . $args[1];
    } else {

      $queryWhereArguments .= $this->clusterFieldsAndValues (  $whereCondition );
    }

    return $queryWhereArguments;
  }

  public function selectTable ( $tableName, $whereCondition = '' )
  {

    $querySelect = "SELECT * FROM $tableName";

    if ( $whereCondition != '' )
      $querySelect .= $this->whereCondition ( $whereCondition );

    $querySelect .= ';';

    return $this->queryTransaction ( $querySelect );
  }


  public function updateTable ( $tableName, $fieldsValued, $whereCondition )
  {

    $queryUpdate = "UPDATE $tableName SET ";

    $queryUpdate .= $this->clusterFieldsAndValues ( $fieldsValued );

    $querySelect .= $this->whereCondition ( $whereCondition );

    $queryUpdate .= ';';

    $this->queryTransaction ( $queryUpdate );
  }

  public function deleteIntoTable ( $tableName, $whereCondition )
  {

    $queryDelete = "DELETE FROM $tableName";

    $querySelect .= $this->whereCondition ( $whereCondition );

    $queryDelete .= ';';

    $this->queryTransaction ( $queryDelete );
  }

  public function insertIntoTable ( $tableName, $fieldsValued )
  {

    $queryInsert = "INSERT INTO $tableName ";

    $arrayCells =  $this->separeteFieldsAndValues ( $fieldsValued );

    $queryInsert .= $arrayCells[0] . ' VALUES ' . $arrayCells[1];

    $queryInsert .= ';';

    $this->queryTransaction ( $queryInsert );
  }

  private function instanceDatabase (  )
  {

    $queryDbCreate = "CREATE DATABASE $this->stdDatabase ; ";

    ////
    ///  Standard Table
    $queryTableCreate = 'CREATE TABLE '. $this->stdDatabase . ' ( '.
                        'id INT (11) NOT NULL AUTO_INCREMENT, '.
                        'name CHAR (50) NOT NULL, '.
                        'password CHAR (60) NOT NULL, '.
                        'PRIMARY KEY ( id ) );';

    $this->queryTransaction ( $queryDbCreate, array (  ), false, false );

    $this->resetConection (  );

    $this->queryTransaction ( $queryTableCreate, array (  ), false, false );

  }

  private function checkDatabase (  )
  {

    $queryListDatabases = "SHOW DATABASES;";

    $databases = $this->queryTransaction ( $queryListDatabases, array (  ), true, false );

    $search = true;

    foreach ( $databases as $value ) {

      if ( $value['Database'] == $this->stdDatabase )
        $search = false;
    }

    if ( $search )
      $this->instanceDatabase (  );
  }

  public function __construct ( $instantStartup = true, $stdDatabase = ( self::DATABASE_STD ), $hostHospededBase = ( self::HOST_DATABASE ), $userName = ( self::USER_DATABASE_NAME ),
                                  $passwdLoginUser = ( self::USER_DATABASE_PASSWD ),  $databaseType = ( self::TYPE_DATABASE ) )
  {

    $this->hostHospededBase = $hostHospededBase;
    $this->userName = $userName;
    $this->passwdLoginUser = $passwdLoginUser;
    $this->databaseType = $databaseType;
    $this->stdDatabase = $stdDatabase;

    if ( $instantStartup )
      $this->checkDatabase (  );
  }

  public function __destruct (  )
  {

    try {

      $this->desconectDatabase (  );
    } catch ( \Exception $error ) {

      ( new MyError )->checkTypeError ( $error );
    }
  }

}
